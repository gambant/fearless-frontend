import React, { useState, useEffect } from 'react';

function AttendConferenceForm () {
    // Set the useState hook to store "name" in the component's state,
    // with a default initial value of an empty string.
    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    // const [company_name, setCompanyName] = useState('');
    const [conference, setConference] = useState('');
    const [hasSignedUp, setHasSignedUp] = useState(false);
    // const [conferences, setConferences] = useConferences(props.conferences)

    // const getFetch = async () => {
    //     const url = 'http://localhost:8000/api/conferences/';
    //     const response = await fetch(url);
    //     if (response.ok) {
    //     const data = await response.json();
    // }

    // useEffect(() => {

    //     getFetch();
    //   }, []);

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    // const handleCompanyNameChange = (event) => {
    //     const value = event.target.value;
    //     setCompanyName(value);
    // }
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.name = name;
        data.email = email;
        data.conference = conference;
        // console.log(data);


        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            // const newAttendee = await response.json();
            // console.log(newAttendee);

            setName('');
            setEmail('');
            // setCompanyName('');
            setConference('');
            setHasSignedUp(true);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';


        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            // console.log(data);
            setConferences(data.conferences);

        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (conferences.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSignedUp) {
      messageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                    <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                        <form className={formClasses} onSubmit={handleSubmit} id="create-attendee-form">
                                <h1 className="card-title">It's Conference Time!</h1>
                                <p className="mb-3">
                                    Please choose which conference
                                    you'd like to attend.
                                </p>
                                <div className={spinnerClasses} id="loading-conference-spinner">
                                    <div className="spinner-grow text-secondary" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <select className={dropdownClasses} onChange={handleConferenceChange} name="conference" id="conference" required>
                                        <option value="">Choose a conference</option>
                                        {conferences.map(conference => {
                                            return (
                                                <option key={conference.href} value={conference.href}>
                                                    {conference.name}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <p className="mb-3">
                                    Now, tell us about yourself.
                                </p>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                                            <label htmlFor="name">Your full name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                                            <label htmlFor="email">Your email address</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">I'm going!</button>
                            </form>
                            <div className={messageClasses} id="success-message">
                                Congratulations! You're all signed up!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AttendConferenceForm;
